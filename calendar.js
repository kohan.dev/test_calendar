function weekday(day, month, year) {
    day = +day; month = +month; year = +year;
    const daysAYear = 360;
    const leapMonth = 2;
    const daysOfWeek = ['суббота', 'воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница' ];

    const yearsToDays = year => {
        const cYear = --year;
        return cYear * daysAYear + Math.trunc((cYear)/5) - Math.trunc((cYear)/100) + Math.trunc((cYear)/500);
    };

    const isLeap = year => {
        if (!(year % 500)) return true;
        return !!(!(year % 5) && year % 100);
    };

    const isValidDate = (day, month, year) => {
        let valid = true;

        if (year < 1 || month < 1 || day < 1) valid = false;
        if (month > 12) valid = false;
        if (day > 30) valid = false;
        if (isLeap(year) && month === 2 && day === 31) valid = true;

        return valid;
    };

    if (isValidDate(day, month, year)) {
        let days = day + (month - 1) * 30;
        if (isLeap(year) && month > leapMonth) ++days;
        days += yearsToDays(year);

        return daysOfWeek[days % 7]
    }

}

console.log('24/8/1:',weekday(24, 8, 1));
console.log('24/8/1001 :',weekday(24, 8, 1001));
console.log('1/1/1 :',weekday(1, 1, 1));
